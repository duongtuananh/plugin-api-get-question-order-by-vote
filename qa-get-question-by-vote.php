<?php
const DEFAULT_SORT_BY = 'DESC';
const DEFAULT_NUMBER_PER_PAGE = 10;
class qa_get_question_by_vote
{
    function match_request($request)
    {
        $apiUrl = 'api/get-question-by-vote';
        return $request === $apiUrl;
    }

    protected function response($status_code, $data = NULL)
    {
        header($this->_build_http_header_string($status_code));
        header("Content-Type: application/json");
        echo json_encode($data);
        die();
    }

    private function _build_http_header_string($status_code)
    {
        $status = array(
            200 => 'OK',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error'
        );
        return "HTTP/1.1 " . $status_code . " " . $status[$status_code];
    }

    function process_request($request)
    {
        $isValidMethod = $this->checkMethodRequest('GET');
        if (!$isValidMethod) {
            $this->response(404, 'This route not support method ' . $_SERVER['REQUEST_METHOD']);
        }
        $sortBy = $this->getRouteParam('sort-by');
        if (!$sortBy) {
            $sortBy = DEFAULT_SORT_BY;
        }
        $perPage = $this->getRouteParam('perpage');
        if (!$perPage) {
            $perPage = DEFAULT_NUMBER_PER_PAGE;
        }
        $query = $this->getQueryGetPost($sortBy, $perPage);
        $results = qa_db_query_raw($query);
        $dataAssoc = qa_db_read_all_assoc($results);
        $dataResponse = $this->getDataResponse($dataAssoc);
        $this->response(200, $dataResponse);
    }

    private function getRouteParam($nameParam)
    {
        return qa_get($nameParam);
    }

    private function getDataResponse($dataAssoc)
    {
        $dataResponse = [];
        foreach ($dataAssoc as $item) {
            $dataResponse[] = [
                'post_id' => $item['postid'],
                'title' => $item['title'],
                'content' => $item['content'],
                'net_votes' => $item['netvotes']
            ];
        }
        return $dataResponse;
    }

    private function getQueryGetPost($sortBy, $perPage) {
        $prefixTableDB = constant('QA_MYSQL_TABLE_PREFIX');
        $table_post = $prefixTableDB . 'posts';
        return 'select * from '. $table_post .' order by netvotes ' . $sortBy . ' limit ' . $perPage;
    }

    private function checkMethodRequest($method)
    {
        return $method === $_SERVER['REQUEST_METHOD'];
    }

}