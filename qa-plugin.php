<?php
qa_register_plugin_module(
    'page', // type of module
    'qa-get-question-by-vote.php', // PHP file containing module class
    'qa_get_question_by_vote', // name of module class
    'Get Question By Vote Number' // human-readable name of module
);